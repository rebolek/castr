Red [
    title: "Basic Secure TCP test server"
]

cert: {
-----BEGIN CERTIFICATE-----
MIIEOTCCAyGgAwIBAgIIXBtk0AsQg+wwDQYJKoZIhvcNAQELBQAwgYQxCzAJBgNV
BAYTAkNOMRUwEwYDVQQKEwxiaXRiZWdpbi5jb20xHTAbBgNVBAsTFGJpdGJlZ2lu
IHRlc3QgcnNhIGNhMRwwGgYDVQQDExNyc2EuY2EuYml0YmVnaW4uY29tMSEwHwYJ
KoZIhvcNAQkBDBJiaXRiZWdpbkBnbWFpbC5jb20wHhcNMjAwNzMwMDMzNzM5WhcN
MjEwNzMwMDMzNzM5WjCBiDELMAkGA1UEBhMCQ04xFTATBgNVBAoTDGJpdGJlZ2lu
LmNvbTEfMB0GA1UECxMWYml0YmVnaW4gdGVzdCByc2EgdGVzdDEeMBwGA1UEAxMV
cnNhLnRlc3QuYml0YmVnaW4uY29tMSEwHwYJKoZIhvcNAQkBDBJiaXRiZWdpbkBn
bWFpbC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDbCFcBITvA
7hKQ17C+J8uOUeXJjg9DJu7k0XW8sspWoOeSfH9uSZarJ5Y8mvOdiotzB30Ws2CY
QMoIPU7AX0fxR+L14ax2FXpZIBMN5Igie3AxQmUG35Z8jU6vPt5nsiMNqlqI2UYG
bbqiSCfoJYIRURVOb2+RTMMq9wjfqQw8Be9d0JEFDhjubJQg8zSrI3cZloH6pwQE
mLjhypYBVcag8H7dWjAcd2Av7mVKl+tXOW/zJ0Gv/iqFIJQdmH1QtyItgGdXAL8A
E+UBqdT+An0URz3nX2YARx1CIwGaEHhZr8NSwMq002bRtMBgtvpgc/owZ9iEErX9
CdfWJ11pGbYDAgMBAAGjgagwgaUwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQG
CCsGAQUFBwMBBggrBgEFBQcDAjAdBgNVHQ4EFgQU/OaWf+XbMo8TOJcAyErc+n7A
7powHwYDVR0jBBgwFoAUqf5pqJzmUIktVgda6Dry/Y0gth8wNAYDVR0RBC0wK4IV
cnNhLnRlc3QuYml0YmVnaW4uY29tgRJiaXRiZWdpbkBnbWFpbC5jb20wDQYJKoZI
hvcNAQELBQADggEBAE/ZqO5aNXSfpHFV1lm/ibTr61zhfRSoS45Y9Dzd6M36ZcGr
15DhKHDkCyiwl2NiZD7vvkmtfF/bxxy6XYSvDXuHTxLBBzJfBKbqaNU3tO/U7Q4y
7XmYlPU2OhyLgJlWLOLHC+oT4gZFxoNkORQ9sWO9qLs2KWMR0Ix0OBha+Jr9moNS
Y6F3cK9rz1AbDKr44n6EtGtyFZDJRxS4fEjn6rQxovo4jT3BFGHEvZSHKQmTq33A
g3/OEt7yGY1loAGqhwGv5/XsjUi7c4OqoUtcYa+Nu0LTPC4jqDa2vImVr8lOeQhb
1CbRz5A+w9N+bJ50Rw2G1uiJU9npk+5pzz4mEQc=
-----END CERTIFICATE-----
}

chain: {
-----BEGIN CERTIFICATE-----
MIIESDCCAzCgAwIBAgIIJoJioe5Xq7gwDQYJKoZIhvcNAQELBQAwgYgxCzAJBgNV
BAYTAkNOMRUwEwYDVQQKEwxiaXRiZWdpbi5jb20xHzAdBgNVBAsTFmJpdGJlZ2lu
IHRlc3QgcnNhIHJvb3QxHjAcBgNVBAMTFXJzYS5yb290LmJpdGJlZ2luLmNvbTEh
MB8GCSqGSIb3DQEJAQwSYml0YmVnaW5AZ21haWwuY29tMB4XDTIwMDczMDAzMzYy
MVoXDTIxMDczMDAzMzYyMVowgYQxCzAJBgNVBAYTAkNOMRUwEwYDVQQKEwxiaXRi
ZWdpbi5jb20xHTAbBgNVBAsTFGJpdGJlZ2luIHRlc3QgcnNhIGNhMRwwGgYDVQQD
ExNyc2EuY2EuYml0YmVnaW4uY29tMSEwHwYJKoZIhvcNAQkBDBJiaXRiZWdpbkBn
bWFpbC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC1F909bb1a
vnxSKIx+vFrDAXKB79Y+OT2XZLUCpYRRrTJpVTv/yfyRuEtYg7SsUDsdBVrebF9M
LVd6bowe7qekkSCDdhzBeDkyQVMbHZODz6GLW9cPOivWGFjPfChjroOX6L36x78P
4XvlqAka8+rWRteNre+fRnHbB443QDNI2zV0iY6pD9OioqNXZ1sAvTf9ruK8hwR2
GSMnG2SUtUvD70UQF/m9RNTok9jZVmes1rwW68nkqJBa2wCy2E9VpoX72fjnR2eU
OxLLkBpqc6EtpieWkvQQ4BGy0YoVAdLr6/pbWzOZqIg3pnGNwnQbyLSFWH+P0Lp/
PZFZSRPUcMx1AgMBAAGjgbcwgbQwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQG
CCsGAQUFBwMBBggrBgEFBQcDAjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBSp
/mmonOZQiS1WB1roOvL9jSC2HzAfBgNVHSMEGDAWgBRKsjiyxs0LKPSUGChglCC/
EFb28TAyBgNVHREEKzApghNyc2EuY2EuYml0YmVnaW4uY29tgRJiaXRiZWdpbkBn
bWFpbC5jb20wDQYJKoZIhvcNAQELBQADggEBAB9JLHIWFnCsqogxZ4dRkyXEtGDF
dsefaLtUf+R1mjOQd7ns4L9oPap3f+n9drw4pokxXL3+HOC9pkzaU3bCajmOYhl2
K+OJefC6E+rCrh/9FhvRn09Jt8WUDrHZ7+hXAa9pUS6Shl6v/LSTay9GBpudidgy
FQowC5nRDmdI08yBHlSlblg7zw8PRnANcvJbZScpHNEPsJsE8mxvxiAJqWrTL+dD
zX9R1ROr5EWbnXV6Tm+FWR352isyWIcmye0FZ9PzhETJ4WaT2KowJ0bbsHS+GGTV
oCBWtJdAzVvXJqOqJ4tQj+izJODndGXI9LQ4Sfga8h94oDeb8otlAmlP+6k=
-----END CERTIFICATE-----
}

key: {
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA2whXASE7wO4SkNewvifLjlHlyY4PQybu5NF1vLLKVqDnknx/
bkmWqyeWPJrznYqLcwd9FrNgmEDKCD1OwF9H8Ufi9eGsdhV6WSATDeSIIntwMUJl
Bt+WfI1Orz7eZ7IjDapaiNlGBm26okgn6CWCEVEVTm9vkUzDKvcI36kMPAXvXdCR
BQ4Y7myUIPM0qyN3GZaB+qcEBJi44cqWAVXGoPB+3VowHHdgL+5lSpfrVzlv8ydB
r/4qhSCUHZh9ULciLYBnVwC/ABPlAanU/gJ9FEc9519mAEcdQiMBmhB4Wa/DUsDK
tNNm0bTAYLb6YHP6MGfYhBK1/QnX1iddaRm2AwIDAQABAoIBAQCy5n/x60wcDBhb
9+BjoKtxZlEyrHao2a78dF0fMEV2NlyO2GNZGIAY8e+TNtjOj6/Q14gDN6YpFZou
2qxtEiX33Ehzns03cdmJ3FnT2hqbV4lOorVTxOHsf5qKJuaPef+JlN/3sMeyToPH
Mvptcd1n+XtWokxVOIacnEPCMxhMGOYTORcfb6JpmbX6YBMfi7oLuOC41GX4XMpI
GRicgGbudAvCTjNwyur5e4aiZb5+TwlA4lK93GcDmHM5Ry1w+JiXZb+6ljHaS6n9
4HBQaPWsacCUEiqc040cejIZfBfwBd9s/3s/vOb75Hn7XxnjF9N+HEs16f28smNc
wJ6tqrLJAoGBAO3KzVC47J8toOPrRU6GbQIXtBuMmCeJp/jdZWgoxe7IWSpczY5a
CruW5asJXezCwPpPQCpm/Jz05P9BJ2lAm04eoInSYjvBtnPj83Ac8ZGLzEwxcVZ6
lErco3E/kMwhqJnCeM3d+98zoK3/BZVKL31o6h/lgWjWDJfL8QQe9Kb/AoGBAOvN
z+tfcYeirNfv5y0S5N4x/wkLWP2BFQ4tZykQvwA4YQdyEEBCdWLrHiSQeFHmTizX
lEK5LE3O+ymmOw1lPbHrxQaLjKEETu42gefcm4ePFTFWWB9lL8X570UKe4is03PK
7ctOaYBL6eQi0U/N25YVYkrRax/tFmVhcIeye1T9AoGBALik1mLNBWfg3WK9iJae
CT0rvL37oRZqbJTxDVIg/vkYdyxO3Yqg3l6loHb8EgPLgl+bdD64s4UJIFwYcorn
EPFLFOuQAplWJN++bIRs5SNYQ24Somg9TLMIup0wd7YamLm0aWup6G97vlP9h49S
CKg/9DSKWL9YHnUsdlz10JFvAoGACq+nDomiwXzTzG1o0pgamx806ZoEDGj/JUdV
d3z0yvPDIe0/Y0ofu73f/dQ2P6/qNwoGBAgUQ+pvY3HPF0pRQ3Dl2ugUFW/2i8EY
ngNXYQtQ8I1zLOkPAy/SchSJiKYcZxD5Oc9x5IiMLvSM+8reeqy5aZ6X9jhuXWXP
Dbc+85kCgYAoyYcpNbVG96/7eV4Dtf0B9EInQoSOBFz4ymVjPQvgYA5l+H/nc3uy
Hln4oGukwteqlhnk/BLrqbndgcNBYq/aCAUXuurHKkJPXWuRAgzA86rnnfemmfL4
kOXkDJTRdJrs5wMGiwvE5qH3L6FFbi+czciJzXnbn5dyAMWfBkgc6g==
-----END RSA PRIVATE KEY-----
}

#include %rs-common.red

protos: [
	sslv3	0300h
	tls1.0	0301h
	tls1.1	0302h
	tls1.2	0303h
	tls1.3	0304h
]

debug: :print
;debug: :comment

data: make binary! 1000
state: 'wait

reply: none

rules: [
	'ping (reply: 'pong)
|	'stop (halt)
]

load-rules: func [rules file] [
	unless exists? file [
		do make error! "File does not exist"
	]
	repend rules ['| make-rules skip load file 2]
]

payload-info: [
	type:			red
	compression:	deflate
]

process-payload: func [payload] [
set 'ply payload
print #process-payload
	; decompress
	if header/compression [
		payload: decompress payload header/compression
	]
	; convert
	payload: switch header/type [
		red		[load payload]
		redbin	[load/as payload 'redbin]
	]
	; check KEEP flag
	probe header
	if header/keep [
		payload-info/compression: header/compression
		payload-info/type: header/type
	]
	probe payload
]

process-command: func [dialect] [
print #process-command
probe dialect
	if word? dialect [dialect: reduce [dialect]]
	reply: make error! "unknown-command"
	parse dialect rules
	if word? reply [reply: reduce [reply]]
	probe payload-info
	set [header reply] make-payload reply payload-info/type payload-info/compression
	rejoin [header reply]
]

process-data: func [port] [
	;debug ["port data:" port/data]
	debug "process-data enter"

	switch state [
		wait [
			print port/data
			either header: process-header port/data data [
				state: 'receive
				copy port
			] [
				insert port "Go away"
			]
		]
		receive [
			print #recieve
			append data port/data
			state: 'inserted
			if equal? header/length length? data [
				state: 'wait
				insert port process-command process-payload data
			]
		]
		inserted [
			copy port
		]
		received [
		]
	]

]

new-event: func [event] [
	debug ["=== Subport event:" event/type]
	switch event/type [
		read  [process-data event/port]
		wrote [close event/port]
	]
]

n: 0
new-client: func [port] [
	debug ["=== New client ===" n]
	n: n + 1
	clear data
	port/awake: :new-event
	copy port
]

server: open tls://:8123

server/extra: compose/deep [
	certs: [(cert) (chain)]
	key: (key)
	;password: "mypass"
	min-protocol: (protos/tls1.1)
	max-protocol: (protos/tls1.3)
]

server/awake: func [event] [
    if event/type = 'accept [new-client event/port]
]

print "Secure TCP server: waiting for client to connect"

;#include %actions-users.red
load-rules rules %rules-users.red
;#include %actions-polls.red
load-rules rules %rules-polls.red

if none? system/view [
	wait server
	print "done"
]
