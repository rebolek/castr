# LEST

*Lest* is a templating system. Lest pages are written in a dialect described below.

## settings

### NO-CACHE

Override cache settings

### PAGE

Force `/page` refinement

### SNIPPET

Ignore `/page` refinement

## header

### TITLE

Set page title.

Usage:
```
title "Page generated by Lest"
```

### DESCRIPTION

Set page description. Alternative keyword: `desc`.

Usage:
```
desc "Page descritpion"
```

### CSS

Add a cascade style sheet to the page. Multiple styles can be added.

Usage:
```
css %css/local.css
css http://www.example.com/remote.css
```
### JS

Add Javascript source to the page. Multiple scripts can be added.

Usage:
```
script %js/local.js
script https://www.example.com/remote.js
```

### 

## body
