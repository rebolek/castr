# CASTR - Client And Server Tools for Red

CASTR is collection of tools intened to make live on HTTP protocol easier for Reducers (Red users). it started its live as part of my [Red Tools](https://github.com/rebolek/red-tools) collection, but due to number of tools in that repo it makes sense to move it to new place.

The amazing CASTR logo was done by @wallysilva. 

## Common tools

TBD

## Client side tools

### MAKE-REQUEST and SEND-REQUEST

TBD: see docs

## Server side tools

### MAKE-RESPONSE and SEND-RESPONSE

TBD: see docs

### TRANSFORM

TBD: see docs

### USERS

TBD see docs
