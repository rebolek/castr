Red[
	Title: "Lest page templates"
	Author: "Boleslav Březovský"
]

%404 [
	title "404 Not found"
	desc "404 not found"
	h1 "we really tried"
	p "But the file can't be found anywhere. We are deeply sorry."
]
%index [
	title "Use HUB!"
	desc "HUB is great!"
	h1 "HUB is wonderful webserver!"
	p "But you probably should make your own index page. See docs (TODO: must be simply accessible from here)"
]
